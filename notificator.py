import datetime
import requests
import gitlab
from lxml import html
import os
import telegram as t

LAST_HASH_KEY = "LAST_HASH"


def main(league):
    print("Start checking.")
    date_hash = 0

    previous_hash = get_var(LAST_HASH_KEY + league)

    print("Previous hash time is {}".format(previous_hash))

    try:
        page = requests.get('https://www.foxsports.com/soccer/schedule?competition={}'.format(league))
        tree = html.fromstring(page.content)
        data_list = tree.xpath('//section[@class="wisbb_body"]//tr/th/text() | '
                               '//section[@class="wisbb_body"]//span[@class="wisbb_status"]/text()')
        comp_name = str(tree.xpath('//span[@class="wisbb_pageInfoSecondaryText"]/text()')[0])

        try:
            for value in data_list:
                data_element = str(value).split(" ")

                if len(data_element) == 3:
                    date_hash = get_date_hash(data_element[2], data_element[1])

                if len(data_element) == 2:
                    time_hash = get_time_hash(data_element[0])
                    hash_code = date_hash + time_hash
                    current_hash = get_current_hash()
                    # todo fix this. WA for getting compatible time between the pipeline runner and foxsports.com
                    hash_code += 4
                    process_notification(current_hash, hash_code, previous_hash, comp_name, league)

                    if 24 > current_hash - previous_hash and 0 <= hash_code - current_hash < 2:
                        send_notification(hash_code - current_hash, comp_name)
                        update_variable(current_hash - 24, league)


                    break

        except Exception as e:
            print("Unknown error. Contact the developer. Information about the error is : {}".format(e))
            raise e

    except Exception as e:
        print("Problem to connect to the site for parsing the schedule of matches."
              " Error is {}".format(e))
        raise e


def get_list_of_mounts():
    return {"July": 31,
            "August": 30,
            "September": 31,
            "October": 30,
            "November": 31,
            "December": 30,
            "January": 31,
            "February": 28 + (
                    datetime.datetime.now().year % 4 == 0 and datetime.datetime.now().year % 400 != 0),
            "March": 31,
            "April": 30,
            "May": 31,
            "June": 30}


def get_date_hash(day, mounth):
    hash_code = 0
    for key, value in iter(get_list_of_mounts().items()):
        if key != mounth:
            hash_code += 24 * value
        else:
            hash_code += int(day) * 24
            break
    return hash_code


def get_time_hash(t):
    postfix = str(t)[-1]
    hour = int(str(t).split(":")[0])
    if postfix == 'p' and hour < 12:
        hour += 12
    return hour


def get_current_hash():
    hash = 0
    now = datetime.datetime.strptime(requests.get('http://just-the-time.appspot.com/').text.strip(),'%Y-%m-%d %H:%M:%S')
    month = (now.month + 5) % 12
    for index, value in enumerate(get_list_of_mounts().values()):
        if index < month:
            hash += 24 * value
    hash += now.day * 24
    hash += now.hour

    return hash


def send_notification(time_before, comp_name):
    try:
        TELEGRAM_TOKEN = os.getenv("TELEGRAM_TOKEN")
        bot = t.Bot(token=TELEGRAM_TOKEN)

        try:
            bot.send_message(chat_id=os.getenv("CHAT_ID"),
                             text="Ставим ставочки, следующий матч менее"
                                  " чем через {} ч.\n {}".format(time_before, comp_name))
            print("Notification delivered")

        except Exception as e:
            print("An error with delivering. Error is {}".format(e))
            raise e

    except Exception as e:
        print("Bot creation exception. Error is {}".format(e))
        raise e


def process_notification(current_hash, hash_code, previous_hash, comp_name, league):
    print("Current hash time is {}".format(current_hash))
    print("Next match hash time is {}".format(hash_code))
    if hash_code - previous_hash >= 24 * 4 and hash_code - current_hash < 1 * 24:
        send_notification(hash_code - current_hash, comp_name)
        update_variable(current_hash, league)


def update_variable(current_hash, league):
    git = gitlab.Gitlab('https://gitlab.com', private_token=os.getenv('ACCESS_TOKEN'))
    project_id = git.projects.get(os.getenv('CI_PROJECT_ID'))
    try:
        project_id.variables.delete(LAST_HASH_KEY + league)
    except Exception:
        print("Strange thing's happened. There was not CI/CD  variable with LAST_HASH."
              " I'm gonna create a new one")
    finally:
        project_id.variables.create({'key': LAST_HASH_KEY + league, 'value': current_hash})
        print("Congrats, I created a new one with the hash time value = {}", current_hash)


def get_var(key):
    previous_hash = os.getenv(key)
    if not previous_hash:
        previous_hash = 0
    else:
        previous_hash = int(previous_hash)
    return previous_hash


if __name__ == '__main__':

    for l in os.getenv("LEAGUES").split(","):
        main(l)
